#include <iostream>
#include <conio.h>
#include <windows.h>

using namespace std;
const int wid = 30, hig = 30;
struct Point
{
	int x, y;
};
void gotoxy(int x, int y)
{
	COORD coord;
	coord.X = x;
	coord.Y = y;
	SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE), coord);
}

class Snake
{
public:
	struct Point A[100], del, apple;
	int length, Alive;
	Snake() {
		Alive = 1;
		apple.x = rand() % wid;
		apple.y = rand() % hig;
		length = 3;
		del.x = 0;
		del.y = 0;
		A[0].x = 10; A[0].y = 10;
		A[1].x = 11; A[1].y = 10;
		A[2].x = 12; A[2].y = 10;
	}
	void Update() {
		gotoxy(0, 0);
		cout << "Chieu dai: " << length;
		gotoxy(del.x, del.y);
		cout << " ";
		for (int i = 0; i < length; i++) {
			gotoxy(A[i].x, A[i].y);
			if (i == 0)
				cout << "X";
			else cout << "O";
		}
	}
	void drawApple()
	{
		gotoxy(apple.x, apple.y);
		cout << "*";
	}
	void Growth()
	{
		length++;
	}
	void Move(int direction) {
		if (A[0].x == apple.x && A[0].y == apple.y)
		{
			this->Growth();
			gotoxy(apple.x, apple.y);
			cout << " ";
			apple.x = rand() % wid;
			apple.y = rand() % hig;
			this->drawApple();
		}
		for (int i = length - 1; i > 0; i--)
		{
			if (i == length - 1)
			{
				del.x = A[i].x;
				del.y = A[i].y;
			}
			A[i] = A[i - 1];
		}
		if (direction == 4)
		{
			A[0].x = A[0].x + 1;
			if (A[0].x > wid)
			{
				A[0].x = 1;
			}
		}
		if (direction == 3)
		{
			A[0].y = A[0].y + 1;
			if (A[0].y > hig)
			{
				A[0].y = 1;
			}
		}
		if (direction == 2)
		{
			A[0].x = A[0].x - 1;
			if (A[0].x < 1)
			{
				A[0].x = wid;
			}
		}
		if (direction == 1)
		{
			A[0].y = A[0].y - 1;
			if (A[0].y < 1)
			{
				A[0].y = hig;
			}
		}
		for (int i = 1; i < length; i++)
		{
			if (A[0].x == A[i].x && A[0].y == A[i].y && length >= 5)
				Alive = 0;
		}
	}
};

int getDirection(char c, int predirection)
{
	switch (c)
	{
	case 'w':
		if (predirection != 3) return 1;
		return predirection;
		break;
	case 'a':
		if (predirection != 4) return 2;
		return predirection;
		break;
	case 's':
		if (predirection != 1) return 3;
		return predirection;
		break;
	case 'd':
		if (predirection != 2) return 4;
		return predirection;
		break;
	default:
		break;
	}
	return predirection;
}
void drawMap()
{
	system("cls");
	for (int i = 0; i <= wid; i++)
	{
		gotoxy(i, 0);
		cout << "#";
		gotoxy(i, hig + 1);
		cout << "#";
	}
	for (int i = 0; i <= hig; i++)
	{
		cout << "#";
		gotoxy(wid + 1, i);
		cout << "#\n";
	}
}
void gameLoop()
{
	drawMap();
	Snake r;
	r.drawApple();
	int predirection = 0;
	int direction = 0;
	while (r.Alive)
	{
		if (_kbhit())
		{
			direction = getDirection(_getch(), predirection);
		}
		predirection = direction;
		r.Update();
		r.Move(direction);
		Sleep(100);
	}
	cout << "Game over";
	system("pause");
}
void Run()
{
	int flag = 1;
	do
	{
		system("cls");
		cout << "GAME MENU:\n1.Choi\n2.Thoat\nNhap lua chon cua ban: ";
		cin >> flag;
		switch (flag)
		{
		case 1:
			gameLoop();
			break;
		default:
			break;
		}
	} while (flag != 2);
}
int main()
{
	Run();
	return 0;
}